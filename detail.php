<?php
    require_once('src/login.php');
    require_once('src/functions.php');
    ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Crews Detail</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
        <div class="container-fluid">
            <?php
                //get group array
                $group = groupForID($_GET['id']);
                $name = $group->name;
                $campus = $group->campus;
                $description = $group->description;
                $meetDay = $group->meeting_day;
                $meetTime = $group->meeting_time;
                $leaders = $group->main_leader->full_name;
                $address = $group->addresses->address[0];
                $address_name = $group->area;
                $crew_additional_info = customFieldWithLabel($group, "Crew Advanced Filters");
                //$group_type = $group->group_type;
                //$childcare_provided = $group->childcare_provided;
                                                 ?>
            <div class="jumbotron centeredtext">
                <h1><?php echo $name;?></h1>
                    <?php /*if (count($leaders) > 0) {
                        echo '<h2><small>';
                        foreach ($leaders as $leader) {
                            echo ' '.$leader['person']['name'].' ';
                        }
                        echo '</small></h2>';
                    }*/
                ?><h2><small><?php echo $leaders;?></small></h2>
                <p class="lead"><?php echo $description;?></p>
                <h4 class="text-<?php echo isGroupOpenAndNotFull($group) ? 'success' : 'danger';?>"><?php echo isGroupOpenAndNotFull($group) ? 'Open' : 'Full';?></h4>
				<?php echo isGroupOpenAndNotFull($group) ? '<p><button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#emailModal" data-crewid="'.$_GET["id"].'" data-crewname="'.$name.'">Join This Crew</button></p>' : '';?>
            </div>
        <div class="row centeredtext">
            <div class="col-md-2">
                <h4>Meets<br><small><?php  /*echo recurrenceStringFromSchedule($schedule)." ".reformatTime($schedule['startTime']);
                            if (!empty($schedule['endTime'])) {
                            echo " - ".reformatTime($schedule['endTime']);
                            }
                            if (!empty($schedule['endDate'])) {
                            //echo '<br>Ends '.reformatDate($schedule['endDate']);
                            }*/ echo $meetDay.' '.$meetTime;?></small></h4>
            </div>
            <div class="col-md-2">
                <h4>Campus<br><small><?php echo $campus;?></small></h4>
            </div>
            <div class="col-md-6">
                <h4>Location<br><small><?php echo $address_name;
                    //if (!is_true($group['isLocationPrivate'])) {
                    echo '<br>';
                    echo reformatLocationAddress($address);
                    //}?></small></h4>
            </div>
            <div class="col-md-2">
                <h4>Additional Info<br><small><?php echo $crew_additional_info;?></small></h4>
            </div>
            <!--<div class="col-md-3">
                <div class="jumbotron centeredtext"><h4>Gender<br><small><?php //echo $group['gender']['name'];?></small></h4></div>
            </div>
            <div class="col-md-3">
                <div class="jumbotron centeredtext"><h4>Marital Status<br><small><?php //echo $group['maritalStatus']['name'];?></small></h4></div>
            </div>
            <div class="col-md-2">
                <h4>Childcare Available<br><small><?php echo (is_true($childcare_provided) ? 'Yes' : 'No');?></small></h4>
            </div>-->
        </div>
        <!-- Time and Location Row -->
        <div class="row">
            <!-- Map -->
            <div class="col-md-12">
                <div class="embed-responsive embed-responsive-16by9">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        </div>
        <?php require('email_modal.php');?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="src/functions.js"></script>
        <script>
            // Initialize and add the map
            function initMap() {
                var providence = {lat: 41.823, lng: -71.412}
                // The map, centered at Providence
                var map = new google.maps.Map(
                    document.getElementById('map'), {zoom: 15, center: providence});

                <?php 
                $formattedAddress = reformatLocationAddress($address);
                $formattedAddress = str_replace("<br>", " ", $formattedAddress);
                $colatedAddress = str_replace(" ", "+", $formattedAddress);
                ?>
                var address = <?php echo '"'.$formattedAddress.'"';?>;
                var geocoder = new google.maps.Geocoder();
                var marker;
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == 'OK') {
                        map.setCenter(results[0].geometry.location);
                        marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            title: <?php echo "\"".$address_name."\"";?>,
                        });
                        marker.addListener('click', function() {
                            window.location.href = "https://www.google.com/maps/dir//<?php echo $colatedAddress;?>";
                        });
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });

                map.addListener('center_changed', function() {
                // 3 seconds after the center of the map has changed, pan back to the
                // marker.
                window.setTimeout(function() {
                    map.panTo(marker.getPosition());
                    }, 3000);
                });
            }
        </script>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6K1flxd2yCNx_OUzS9scQR92iDV0DkRI&callback=initMap">
        </script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </body>
</html>
