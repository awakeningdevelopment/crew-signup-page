/* eslint-disable no-undef */
/* eslint-disable func-names */
// eslint-disable-next-line func-names
(function($) {
  const helper = {
    // custom helper function for debounce - how to work see https://codepen.io/Hyubert/pen/abZmXjm
    /**
     * Debounce
     * need for once call function
     *
     * @param { function } - callback function
     * @param { number } - timeout time (ms)
     * @return { function }
     */
    debounce(func, timeout) {
      let timeoutID;
      // eslint-disable-next-line no-param-reassign
      timeout = timeout || 200;
      return function() {
        const scope = this;
        // eslint-disable-next-line prefer-rest-params
        const args = arguments;
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function() {
          func.apply(scope, Array.prototype.slice.call(args));
        }, timeout);
      };
    },
    /**
     * Helper if element exist then call function
     */
    isElementExist(_el, _cb, _argCb) {
      const elem = document.querySelector(_el);
      if (document.body.contains(elem)) {
        try {
          if (arguments.length <= 2) {
            _cb();
          } else {
            _cb(..._argCb);
          }
        } catch (e) {
          // eslint-disable-next-line no-console
          console.log(e);
        }
      }
    },

    /**
     *  viewportCheckerAnimate function
     *
     * @param whatElement - element name
     * @param whatClassAdded - class name if element is in viewport
     * @param classAfterAnimate - class name after element animates
     */
    viewportCheckerAnimate(whatElement, whatClassAdded, classAfterAnimate) {
      jQuery(whatElement)
        .addClass('a-hidden')
        .viewportChecker({
          classToRemove: 'a-hidden',
          classToAdd: `animated ${whatClassAdded}`,
          offset: 10,
          callbackFunction(elem) {
            if (classAfterAnimate) {
              elem.on('animationend', () => {
                elem.addClass('animation-end');
              });
            }
          }
        });
    },
    // helpler windowResize
    windowResize(functName) {
      const self = this;
      $(window).on('resize orientationchange', self.debounce(functName, 200));
    },
    /**
     * Init slick slider only on mobile device
     *
     * @param {DOM} $slider
     * @param {array} option - slick slider option
     */
    mobileSlider($slider, option) {
      if (window.matchMedia('(max-width: 768px)').matches) {
        if (!$slider.hasClass('slick-initialized')) {
          $slider.slick(option);
        }
      } else if ($slider.hasClass('slick-initialized')) {
        $slider.slick('unslick');
      }
    },
    /**
     * Set cookie
     *
     * @param {string} name
     * @param {string} value
     * @param {int} days
     */
    setCookie(name, value, days) {
      var expires = '';
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = '; expires=' + date.toUTCString();
      }
      document.cookie = name + '=' + (value || '') + expires + '; path=/';
    },
    /**
     *Get Cookie
     *
     * @param {string} name
     * @return {string}
     */
    getCookie(name) {
      var nameEQ = name + '=';
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
      return null;
    },
    /**
     * Erase Cookie,
     *
     * @param {string} name
     */
    eraseCookie(name) {
      document.cookie =
        name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
  };

  const theme = {
    /**
     * Main init function
     */
    init() {
      this.plugins(); // Init all plugins
      this.initAnimations(); // Init all animations
      this.bindEvents(); // Bind all events
    },

    /**
     * Init External Plugins
     */
    plugins() {
      // init jcf select 
      jcf.replace('.jcf-select', 'Select', {
        wrapNative: false,
        wrapNativeMobile: false,
        maxVisibleItems: 5,
        fakeDropInBody: false
      });
    },

    /**
     * Bind all events here
     *
     */
    bindEvents() {
      const self = this;
      /** * Run on Document Ready ** */
      $(document).ready(function() {
        self.smoothScrollLinks();

        helper.isElementExist('.dropdown', theme.initDropdown);
        helper.isElementExist('.tab', theme.initTab);
        helper.isElementExist('.three-cols-cards', theme.initThreeColsCards);
        helper.isElementExist('.masonry-slider__carousel', theme.initMasonrySlider);
        helper.isElementExist('.upcoming-events__slider', theme.initUpcomingEventsSlider);
        helper.isElementExist('.blockquote-slider', theme.initBlockquoteSlider);
        helper.isElementExist('.ministries-grid', theme.initMinistriesSlider);
        helper.isElementExist('.image-carousel', theme.initImageCarousel);
        helper.isElementExist('.accordion', theme.initAccordion);
        helper.isElementExist('.popup', theme.initPopup);

        // init hamburger
        $('.hamburger').on('click', function() {
          $('.header').toggleClass('is-opened');
        });
      });
      /** * Run on Window Load ** */
      $(window).on('scroll', function() {
        if ($(window).scrollTop() >= 50)
          $('.header').addClass('header--sticky');
        else $('.header').removeClass('header--sticky');

        // hide .sticky-btns when .footer is visible
        if (
          $(window).scrollTop() + $(window).height() >
          $(document).height() - $('.footer').outerHeight()
        ) {
          $('.sticky-btns').addClass('is-hidden');
        } else {
          $('.sticky-btns').removeClass('is-hidden');
        }
      });
    },

    /**
     * init scroll revealing animations function
     */
    initAnimations() {
      helper.viewportCheckerAnimate('.a-up', 'fadeInUp');
      helper.viewportCheckerAnimate('.a-down', 'fadeInDown');
      helper.viewportCheckerAnimate('.a-left', 'fadeInLeft');
      helper.viewportCheckerAnimate('.a-right', 'fadeInRight');
      helper.viewportCheckerAnimate('.a-op', 'fade');
    },

    /**
     * Smooth Scroll link
     */
    smoothScrollLinks() {
      $('a[href^="#"').on('click touchstart', function() {
        const target = $(this).attr('href');
        if (target !== '#' && $(target).length > 0) {
          const offset = $(target).offset().top - $('header').outerHeight();
          $('html, body').animate(
            {
              scrollTop: offset
            },
            500
          );
          return false;
        }
      });
    },

    /**
     * init dropdown
     */
    initDropdown() {
      // show dropdown
      $('.dropdown-btn').on('click', function() {
        $(this).closest('.dropdown').toggleClass('is-active');
      });
      // close dropdown
      $('.dropdown-close').on('click', function() {
        $(this).closest('.dropdown').removeClass('is-active');
      });
    },
    /**
     * init tab
     */
    initTab() {
      $('.tab-link').on('click', function() {
        if ( $(this).hasClass('is-active') ) return false;
        const $tab = $(this).closest('.tab');
        const target = $(this).attr('data-target');
        $('.tab-link.is-active').removeClass('is-active');
        $('.tab-content.is-active').removeClass('is-active');
        $(this).addClass('is-active');
        $(target).addClass('is-active');
        theme.initAnimations();
        return false;
      });
    },
    /**
     * init three columns cards slider on mobile
     */
    initThreeColsCards() {
      const $slider = $('.three-cols-cards__grid');
      const option = {
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        variableWidth: true,
        ease: 'Ease'
      };
      helper.mobileSlider($slider, option);
      helper.windowResize(() => {
        helper.mobileSlider($slider, option);
      });
    },
    /**
     * init masonry slider on mobile
     */
    initMasonrySlider() {
      const $slider = $('.masonry-slider__carousel');
      const option = {
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        variableWidth: true,
        ease: 'Ease'
      };
      helper.mobileSlider($slider, option);
      helper.windowResize(() => {
        helper.mobileSlider($slider, option);
      });
    },
    /**
     * upcoming events slider on mobile
     */
    initUpcomingEventsSlider() {
      const $slider = $('.upcoming-events__slider');
      const option = {
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        variableWidth: true,
        ease: 'Ease'
      };
      helper.mobileSlider($slider, option);
      helper.windowResize(() => {
        helper.mobileSlider($slider, option);
      });
    },
    /**
     * init blockquote slider
     */
    initBlockquoteSlider() {
      $('.blockquote-slider').each(function() {
        if ($('.blockquote-slide', this).length>1) {
          $(this).slick({
            arrows: false,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 1000,
            ease: 'Ease',
            fade: true
          });
        }
      });
    },
    /**
     * init ministries slider
     */
    initMinistriesSlider() {
      const $slider = $('.ministries-grid');
      const option = {
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        variableWidth: true,
        ease: 'Ease'
      };
      helper.mobileSlider($slider, option);
      helper.windowResize(() => {
        helper.mobileSlider($slider, option);
      });
    },
    /**
     * init image carousel
     */
    initImageCarousel() {
      $('.image-carousel').slick({
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        variableWidth: true,
        centerMode: true
      });
    },
    /**
     * init accordion
     */
    initAccordion() {
      $('.accordion-heading').on('click', function() {
        const $parent = $(this).closest('.accordion');
        if ($('.accordion.is-opened') != $parent && !$parent.hasClass('is-opened')) {
          $('.accordion.is-opened .accordion-content').slideToggle();
          $('.accordion.is-opened').removeClass('is-opened');
        }
        $parent.toggleClass('is-opened');
        $('.accordion-content', $parent).slideToggle();
      });
    },
    /**
     * init Popup
     */
    initPopup() {
      // Popup show
      $('.btn-popup').on('click', function() {
        const target = $(this).attr('data-target');
        if ($(target).length) {
          $(target).addClass('is-opened');
          $('html, body').css('overflow', 'hidden');
          $(target).trigger({
            type:"show.bs.modal",
            relatedTarget:this
          });
        }
        return false;
      });
      // popup close
      $('.popup-close').on('click', function() {
        $(this).closest('.popup.is-opened').removeClass('is-opened');
        $('html, body').removeAttr('style');
        $(this).closest('.popup.is-opened').trigger('hidden.bs.modal');
      });
    }
  };

  // Initialize Theme
  theme.init();
})(jQuery);
