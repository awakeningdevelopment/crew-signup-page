<?php
    require_once 'src/database.php';
    require_once 'src/models/ccb_list.php';
    require_once 'src/models/crew.php';
    require_once 'src/models/individual.php';
    require_once 'src/models/geocode.php';

    $database = new Database;
    $ccbListTable = new CCBList($database->db);
    $crewsTable = new Crew($database->db);
    $individualsTable = new Individual($database->db);
    $geocodeTable = new GeoCode($database->db);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Crews List</title>
        
        <!-- Bootstrap -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,opsz,wght@0,6..12,200..1000;1,6..12,200..1000&display=swap" rel="stylesheet">
        <link rel='stylesheet' id='am_https-fonts-googleapis-com-css2familyplayfairdisplaydisplayswap-css'
        href='https://fonts.googleapis.com/css2?family=Playfair+Display&#038;display=swap&#038;ver=6.4.1'
        type='text/css' media='all' />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
        <div class="container-fluid">
            <!-- Map -->
            <div id="map"></div>

            <?php
                //CCB pulldown list options
                $campuses = $ccbListTable->getCCBListAsSimpleXML('campus_list')->campus;
                $genders = $ccbListTable->getCCBListAsSimpleXML('udf_grp_pulldown_3_list')->item;//Gender
                $crewTypes = $ccbListTable->getCCBListAsSimpleXML('udf_grp_pulldown_1_list')->item;//Custom Crew Type
                $crewGroupTypes = $ccbListTable->getCCBListAsSimpleXML('group_type_list')->item;//Custom Crew Type
           ?>
            <!-- Filters -->
            <div class="panel panel-default">
                <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <form>
                            <div class="row">
                                <div class="form-group col-md-1 hidden">
                                    <input type="text" class="form-control" id="filterid" placeholder="ID">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="filtername">By Keyword:</label>
                                    <input type="text" class="form-control" id="filtername" placeholder="Title">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="filterlocation">By Location:</label>
                                    <input type="text" class="form-control" id="filterlocation" placeholder="Location">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="filtercrewtype">By Crew Type:</label>
                                    <select class="form-control" id="filtercrewtype">
                                        <option></option>
                                        <?php foreach ($crewTypes as $type): ?>
                                            <option><?= $type->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="filtergender">Gender:</label>
                                    <select class="form-control" id="filtergender">
                                        <option></option>
                                        <?php foreach ($genders as $gender): ?>
                                            <option><?= $gender->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="filterday">Day:</label>
                                    <select class="form-control" id="filterday">
                                        <option></option>
                                        <option>Sunday</option>
                                        <option>Monday</option>
                                        <option>Tuesday</option>
                                        <option>Wednesday</option>
                                        <option>Thursday</option>
                                        <option>Friday</option>
                                        <option>Saturday</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Crew List -->
            <div class="crewlist searchable">
                <?php
                    $crews = $crewsTable->getAllCrewsAsSimpleXML();
                    usort($crews, "Crew::crewAlphabeticalSort");
                    $locations;

                    foreach ($crews as $crew): 
                    // Extract crew details
                    $crewId = $crew->attributes()->id;
                    $crewState = $crew->inactive;
                    $crewName = $crew->name;
                    $crewDescription = $crew->description;
                    $crewMeetingDay = $crew->meeting_day;
                    $crewMeetingTime = $crew->meeting_time;
                    $groupType = $crew->group_type;
                    $crewType = Crew::customFieldWithLabel($crew, "Crew Advanced Filters");
                    $crewGender = Crew::customFieldWithLabel($crew, "Gender");

                    // Extract crew leader details
                    $crewMainLeader = $crew->main_leader;
                    $crewOtherLeaders = $crew->leaders->leader;

                    // Extract crew address details
                    $crewAddress = $crew->addresses->address[0];
                    $crewAddressName = $crew->area;
                    $crewFormattedAddress = Crew::reformatLocationAddress($crewAddress);
                    $crewFormattedAddressNoLB = preg_replace('/\s+/', ' ', str_replace("<br>", " ", $crewFormattedAddress));
                    $crewCollatedAddress = str_replace(" ", "+", $crewFormattedAddressNoLB);
                    $locations[] = [
                        'info' => json_encode("<h4>$crewName</h4>".
                            "<strong>$crewAddressName</strong><br>".
                            "$crewFormattedAddress<br><br>".
                            "<a class=\"directions\" href=\"https://www.google.com/maps/dir//$crewCollatedAddress\">Get Directions</a><br>"),
                        'address' => addslashes($crewCollatedAddress),
                    ];
                    // Determine crew leaders' images
                    $leaders = array_merge([$crewMainLeader], array_map(function($leader) {
                        return $leader;
                    }, iterator_to_array($crewOtherLeaders, false)));
                    ?>
                    <!-- Crew Card -->
                    <div class="crewcard-wrapper filter">
                        <div class="crewcard">
                            <div class="id hidden"><?= $crewId ?></div>
                            <div class="leader-images">
                                <?php foreach ($leaders as $leader): ?>
                                    <div class="leader-image" style="background-image: url('<?= $individualsTable->getCachedImageSrcForIndividual($leader->attributes()->id) ?>');"></div>
                                <?php endforeach; ?>
                            </div>
                            <h5 class="leader-name name">
                                <?php foreach ($leaders as $leader): ?>
                                    <?= $leader->full_name ?>
                                <?php endforeach; ?>
                            </h5>
                            <div class="information">
                                <h2 class="title name"><?= $crewName ?></h2>
                                <h5 class="address"><?= $crewDescription ?></h5>
                                <h5 class="day"><b><?= $crewMeetingDay . ' ' . $crewMeetingTime ?></b></h5>
                                <h5 class="address"><?= $crewFormattedAddress ?></h5>
                                <h5 class="gender hidden"><?= $crewGender ?></h5>
                                <h5 class="crewtype hidden"><?= $crewType ?></h5>
                                <div class="campus hidden"><?= $crew->campus ?></div>
                                <div class="sharerow">
                                    <?php if (Crew::isCrewOpenAndNotFull($crew)): ?>
                                        <button type="button" class="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#emailModal"
                                                data-crewid="<?= $crewId ?>"
                                                data-crewname="<?= $crewName ?>"
                                                data-crewdescription="<?= $crewDescription ?>">
                                            JOIN
                                        </button>
                                    <?php else: ?>
                                        <h5 class="text-danger">Full</h5>
                                    <?php endif; ?>
                                    <a href="javascript:openShare('<?= addslashes($crewId) . "','" . addslashes($crewMeetingDay) . "','" . addslashes($crewMeetingTime) . "','" . addslashes($crewAddressName) ?>')">
                                        <img src="img/share.svg" alt="Share" height="48" width="48">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php require('email_modal.php');?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script   src="https://code.jquery.com/jquery-3.3.1.min.js"   integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="   crossorigin="anonymous"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="src/bootstrap.min.js"></script>
        <script src="src/functions.js?240131"></script>
        <script>
            // Initialize and add the map
            let map;
            async function initMap() {
                const { Map } = await google.maps.importLibrary("maps");
                const providence = { lat: 41.823, lng: -71.412 };
                map = new Map(document.getElementById('map'), { zoom: 11, center: providence });
                const infowindow = new google.maps.InfoWindow({});

                <?php foreach ($locations as $location): ?>
                <?php
                    $address = htmlspecialchars($location["address"], ENT_QUOTES, 'UTF-8');
                    $geocodeJson = $geocodeTable->getGeoCodeForAddress($address);
                    $jsonLocation = false;
                    $info = json_encode('Geocoding failed for ' . $address);

                    if ($geocodeJson) {
                        $geocode = json_decode($geocodeJson);
                        if ($geocode->status === "OK" && isset($geocode->results[0]->geometry->location)) {
                            $jsonLocation = json_encode($geocode->results[0]->geometry->location);
                            $info = $location["info"];
                        }
                    }
                ?>
                addMarkerWithLocationAndInfo(
                    <?= $jsonLocation ?: 'false' ?>, 
                    <?= $info ?>, 
                    map, 
                    infowindow
                );
                <?php endforeach; ?>

                applyFilter(); // Re-run filter after markers are loaded
            }

            function addMarkerWithLocationAndInfo(location, info, map, infowindow) {
                if (!location) {
                    return;
                }

                const marker = new google.maps.Marker({
                    map: map,
                    position: location
                });

                marker.addListener('click', function() {
                    try {
                        infowindow.setContent(info);
                        infowindow.open(map, marker);
                    } catch (e) {
                        console.error('Error parsing JSON:', e);
                    }
                });
                allMarkers.push(marker);
            }
        </script>
        <script async
            src="https://maps.googleapis.com/maps/api/js?key=<?= MAPS_API_KEY?>&loading=async&callback=initMap">
        </script>
    </body>
</html>