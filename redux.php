<?php
    require_once 'src/database.php';
    require_once 'src/models/ccb_list.php';
    require_once 'src/models/crew.php';
    require_once 'src/models/individual.php';
    require_once 'src/models/geocode.php';

    $database = new Database;
    $ccbListTable = new CCBList($database->db);
    $crewsTable = new Crew($database->db);
    $individualsTable = new Individual($database->db);
    $geocodeTable = new GeoCode($database->db);
?><!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title>Crews Signup</title>
    <!-- Preload Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <title>Locations &#8211; awakening-church</title>
    <meta name='robots' content='max-image-preview:large' />
    <link rel='stylesheet' id='am_https-fonts-googleapis-com-css2familyplayfairdisplaydisplayswap-css'
        href='https://fonts.googleapis.com/css2?family=Playfair+Display&#038;display=swap&#038;ver=6.4.1'
        type='text/css' media='all' />
    <link rel='stylesheet' id='am_https-cdnjs-cloudflare-com-ajax-libs-fancybox-3-5-7-jquery-fancybox-min-css-css'
        href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css?ver=6.4.1' type='text/css'
        media='all' />
    <link rel='stylesheet' id='am_assets-css-style-min-css-css' href='assets/css/style.min.css?ver=1701435385'
        type='text/css' media='all' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js?ver=6.4.1"
        id="am_https-cdnjs-cloudflare-com-ajax-libs-slick-carousel-1-8-1-slick-min-js-js"></script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js?ver=6.4.1"
        id="am_https-cdnjs-cloudflare-com-ajax-libs-fancybox-3-5-7-jquery-fancybox-min-js-js"></script>
</head>

<body
    class="page-template page-template-page-templates page-template-locations page-template-page-templateslocations-php page page-id-23 logged-in admin-bar no-customize-support tribe-no-js chrome">
    <!-- Main -->
    <main class="main">

        <!-- Banner -->
        <section class="banner banner--general banner--map">
            <div class="container">
                <div class="banner-left">
                    <div class="banner-top a-op">
                        <div class="breadcrumbs d-md-only">
                            <a href="/">Home</a>
                            <span class="sep">//</span>
                            <span>Crews Signup</span>
                        </div>
                        <ul class="banner-socials">
                            <li>
                                <a href="#" class="banner-social" target="_blank">
                                    <img src="assets/img/icon-youtube.svg" alt="Youtube">
                                </a>
                            </li>
                            <li>
                                <a href="#" class="banner-social" target="_blank">
                                    <img src="assets/img/icon-facebook.svg" alt="Facebook">
                                </a>
                            </li>
                            <li>
                                <a href="#" class="banner-social" target="_blank">
                                    <img src="assets/img/icon-instagram.svg" alt="Instagram">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="map" class="banner-img bg-cover a-op a-delay-1"></div>
                </div>
            </div>
        </section>
        <!-- CPT Grid -->
        <?php
            //CCB pulldown list options
            $campuses = $ccbListTable->getCCBListAsSimpleXML('campus_list')->campus;
            $genders = $ccbListTable->getCCBListAsSimpleXML('udf_grp_pulldown_3_list')->item;//Gender
            $crewTypes = $ccbListTable->getCCBListAsSimpleXML('udf_grp_pulldown_1_list')->item;//Custom Crew Type
            $crewGroupTypes = $ccbListTable->getCCBListAsSimpleXML('group_type_list')->item;//Custom Crew Type
        ?>
        <section class="cpt">
            <div class="container">
                <div class="cpt-filters">
                    <div class="row">
                        <div class="col col-sm-6 col-md-3 hidden">
                            <div class="cpt-filter">
                                <input type="text" name="id" id="filterid" class="cpt-filter-input"
                                    placeholder="ID">
                            </div>
                        </div>
                        <div class="col col-sm-6 col-md-3">
                            <div class="cpt-filter">
                                <label for="keyword">By Keyword:</label>
                                <input type="text" name="keyword" id="filtername" class="cpt-filter-input"
                                    placeholder="Title">
                            </div>
                        </div>
                        <div class="col col-sm-6 col-md-3">
                            <div class="cpt-filter">
                                <label for="location">By Location:</label>
                                <input type="text" name="location" id="filterlocation" class="cpt-filter-input"
                                    placeholder="Location">
                            </div>
                        </div>
                        <div class="col col-sm-6 col-md-3">
                            <div class="cpt-filter">
                                <label for="type">By Crew Type:</label>
                                <select name="type" id="filtercrewtype" class="jcf-select">
                                    <option></option>
                                    <?php foreach ($crewTypes as $type): ?>
                                        <option><?= $type->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col col-sm-6 col-md-3">
                            <div class="row">
                                <div class="col col-6">
                                    <div class="cpt-filter">
                                        <label for="gender">Gender:</label>
                                        <select name="gender" id="filtergender" class="jcf-select">
                                            <option></option>
                                            <?php foreach ($genders as $gender): ?>
                                                <option><?= $gender->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-6">
                                    <div class="cpt-filter">
                                        <label for="day">Day:</label>
                                        <select name="day" id="filterday" class="jcf-select">
                                            <option></option>
                                            <option>Sunday</option>
                                            <option>Monday</option>
                                            <option>Tuesday</option>
                                            <option>Wednesday</option>
                                            <option>Thursday</option>
                                            <option>Friday</option>
                                            <option>Saturday</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row cpt-grid searchable">
                    <?php
                        $crews = $crewsTable->getAllCrewsAsSimpleXML();
                        usort($crews, "Crew::crewAlphabeticalSort");
                        $locations;

                        foreach ($crews as $crew): 
                        // Extract crew details
                        $crewId = $crew->attributes()->id;
                        $crewState = $crew->inactive;
                        $crewName = $crew->name;
                        $crewDescription = $crew->description;
                        $crewMeetingDay = $crew->meeting_day;
                        $crewMeetingTime = $crew->meeting_time;
                        $groupType = $crew->group_type;
                        $crewType = Crew::customFieldWithLabel($crew, "Crew Advanced Filters");
                        $crewGender = Crew::customFieldWithLabel($crew, "Gender");

                        // Extract crew leader details
                        $crewMainLeader = $crew->main_leader;
                        $crewOtherLeaders = $crew->leaders->leader;

                        // Extract crew address details
                        $crewAddress = $crew->addresses->address[0];
                        $crewAddressName = $crew->area;
                        $crewFormattedAddress = Crew::reformatLocationAddress($crewAddress);
                        $crewFormattedAddressNoLB = preg_replace('/\s+/', ' ', str_replace("<br>", " ", $crewFormattedAddress));
                        $crewCollatedAddress = str_replace(" ", "+", $crewFormattedAddressNoLB);
                        $locations[] = [
                            'info' => addslashes("<h4>$crewName</h4>".
                                "<strong>$crewAddressName</strong><br>".
                                "$crewFormattedAddress<br><br>".
                                "<a class=\"directions\" href=\"https://www.google.com/maps/dir//$crewCollatedAddress\">Get Directions</a><br>"),
                            'address' => addslashes($crewCollatedAddress),
                        ];
                        // Determine crew leaders' images
                        $leaders = array_merge([$crewMainLeader], array_map(function($leader) {
                            return $leader;
                        }, iterator_to_array($crewOtherLeaders, false)));
                    ?>
                    <!-- Crew Card -->
                    <article class="col col-6 col-md-3 filter">
                        <div class="id hidden"><?= $crewId ?></div>
                        <div class="loop-location">
                            <div class="loop-location__img">
                                <?php foreach ($leaders as $leader): ?>
                                <div class="loop-location__person">
                                    <img src="<?= $individualsTable->getCachedImageSrcForIndividual($leader->attributes()->id) ?>" alt="">
                                    <p class="name"><?= $leader->full_name ?></p>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="loop-location__content">
                                <p class="loop-location__title name"><?= $crewName ?></p>
                                <div class="loop-location__meta">
                                    <p class="loop-location__address address"><?= $crewDescription ?></p>
                                    <p class="loop-location__time day"><?= $crewMeetingDay . ' ' . $crewMeetingTime ?></p>
                                    <p class="loop-location__address address"><?= $crewFormattedAddress ?></p>
                                </div>
                                <div class="gender hidden"><?= $crewGender ?></div>
                                <div class="crewtype hidden"><?= $crewType ?></div>
                                <div class="campus hidden"><?= $crew->campus ?></div>
                                <div class="loop-location__btns">
                                    <?php if (Crew::isCrewOpenAndNotFull($crew)): ?>
                                        <a  href="#"
                                            class="btn btn-white btn-popup"
                                            data-target="#emailModal"
                                            data-crewid="<?= $crewId ?>"
                                            data-crewname="<?= $crewName ?>"
                                            data-crewdescription="<?= $crewDescription ?>">
                                            Join
                                        </a>
                                    <?php else: ?>
                                        <h5 class="text-danger">Full</h5>
                                    <?php endif; ?>
                                    <a href="javascript:openShare('<?= addslashes($crewId) . "','" . addslashes($crewMeetingDay) . "','" . addslashes($crewMeetingTime) . "','" . addslashes($crewAddressName) ?>')" class="btn-export" aria-label="Export">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13 11L21.2 2.80005" stroke="#0F0F0F" stroke-width="1.5"
                                                stroke-linecap="round" stroke-linejoin="round" />
                                            <path d="M22.0002 6.8V2H17.2002" stroke="#0F0F0F" stroke-width="1.5"
                                                stroke-linecap="round" stroke-linejoin="round" />
                                            <path d="M11 2H9C4 2 2 4 2 9V15C2 20 4 22 9 22H15C20 22 22 20 22 15V13"
                                                stroke="#0F0F0F" stroke-width="1.5" stroke-linecap="round"
                                                stroke-linejoin="round" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <!-- Email Modal -->
        <div class="popup updateModelInfo modal" id="emailModal">
            <div class="popup-inner">
                <div class="popup-heading">
                    <h3 class="popup-title modal-title">Join a Crew</h3>
                    <button class="popup-close" aria-label="Popup Close">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L9 9M9 9L17 1M9 9L1 17M9 9L17 17" stroke="#0F0F0F" stroke-width="1.5" />
                        </svg>
                    </button>
                </div>
                <div class="popup-body">
                    <form id="emailform">
                        <div class="row">
                            <div class="col col-6">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control"
                                        placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col col-6">
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control"
                                        placeholder="Last Name" required>
                                </div>
                            </div>
                            <div class="col col-12">
                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input type="email" name="email" id="email" class="form-control"
                                        placeholder="rsmith@example.com" required>
                                </div>
                            </div>
                            <div class="col col-12">
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input type="text" name="phone" id="phone" class="form-control"
                                        placeholder="555-555-555" required>
                                </div>
                            </div>
                            <div class="col col-12">
                                <div class="form-group">
                                    <label for="birthdate">Birthdate</label>
                                    <input type="date" name="birthdate" id="birthdate" class="form-control"
                                        placeholder="DD/MM/YYYY" required>
                                </div>
                            </div>
                            <div class="col col-12">
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select id="gender" name="gender">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <input id="crewid" name="crewid" type=hidden value="">
                            <div class="col col-12">
                                <button class="btn btn-black btn-submit" type="submit" onclick="SubmitFormAsync();">Join</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="popup" id="successModal">
            <div class="popup-inner">
                <div class="popup-heading">
                    <h3 class="popup-title">Your request has been submitted. You will receive an email once you have been added to the Crew.</h3>
                    <button class="popup-close" aria-label="Popup Close">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L9 9M9 9L17 1M9 9L1 17M9 9L17 17" stroke="#0F0F0F" stroke-width="1.5" />
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </main>
    <!-- /Main -->
    <script type="text/javascript" src="assets/js/libs.min.js" id="am_assets-js-libs-min-js-js"></script>
    <script type="text/javascript" src="assets/js/main.min.js" id="am_assets-js-main-min-js-js"></script>
    <script src="src/functions.js?240131"></script>
        <script>
            // Initialize and add the map
            let map;
            async function initMap() {
                const { Map } = await google.maps.importLibrary("maps");
                const providence = { lat: 41.823, lng: -71.412 };
                map = new Map(document.getElementById('map'), { zoom: 11, center: providence });
                const infowindow = new google.maps.InfoWindow({});

                <?php foreach ($locations as $location): ?>
                    <?php
                    $address = $location["address"];
                    $geocodeJson = $geocodeTable->getGeoCodeForAddress($address);
                    $jsonLocation = false;
                    $info = 'Geocoding failed for ' . htmlspecialchars($address);
                    if ($geocodeJson) {
                        $geocode = json_decode($geocodeJson);
                        if ($geocode->status === "OK" && isset($geocode->results[0]->geometry->location)) {
                            $jsonLocation = json_encode($geocode->results[0]->geometry->location);
                            $info = $location["info"];
                        }
                    }
                    ?>
                    addMarkerWithLocationAndInfo(<?= $jsonLocation ?: 'false' ?>, '<?= $info ?>', map, infowindow);
                <?php endforeach; ?>

                applyFilter(); // Re-run filter after markers are loaded
            }

            function addMarkerWithLocationAndInfo(location, info, map, infowindow) {
                if (!location) {
                    return;
                }

                const marker = new google.maps.Marker({
                    map: map,
                    position: location
                });

                marker.addListener('click', function() {
                    infowindow.setContent(info);
                    infowindow.open(map, marker);
                });
                allMarkers.push(marker);
            }
        </script>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=<?= MAPS_API_KEY?>&callback=initMap">
        </script>
</body>

</html>