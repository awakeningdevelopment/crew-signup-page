<!-- Email Modal -->
<div class="modal fade updateModelInfo" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="emailModalLabel">Join a Crew</h4>
                <h5 class="modal-description" id="emailModalLabel"></h5>
            </div>
            <div class="modal-body">
                <form id="emailform">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-control" name="firstname" placeholder="Robin" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-control" name="lastname" placeholder="Smith" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" class="form-control" name="email" placeholder="rsmith@example.com" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Phone Number</label>
                        <input type="tel" class="form-control" name="phone" placeholder="555-555-5555" required>
                    </div>
                    <div class="form-group">
                        <label for="age">Birthdate</label>
                        <input type="date" class="form-control" name="age" placeholder="12/25/95" required>
                    </div>
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select id="gender" name="gender">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                    <input id="crewid" name="crewid" type=hidden value="">
                    <div id="formalert" class="form-group text-danger">
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"  onclick="SubmitFormAsync();">Send</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" id="successModal" role="dialog" aria-labelledby="successModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="successModalLabel">Your request has been submitted. You will receive an email once you have been added to the Crew.</h4>
            </div>
        </div>
    </div>
</div>