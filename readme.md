## Super quick installation guide

- Place this code on a webserver that is accessible from the internet
- Set up a few mysql tables for storing cached data (because CCB’s
   API is very slow)
	- see SQL below for table structure
- set up a group department within CCB for crews
- set up a CCB API user
- set up a CCB form to receive signups (RSVPs)
- duplicate and rename config-sample.php to config.php and enter your server configuration & ccb information
- set up cron jobs for CCB automation (found in src/services)
	- suggest running "update_group_cache" every 5 mins and "bulkaddtocrews" every 15 minutes

```sql
-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 03, 2020 at 11:37 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crews`
--

-- --------------------------------------------------------

--
-- Table structure for table `application_variables`
--

CREATE TABLE `application_variables` (
`prop` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`var` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ccb_lists`
--

CREATE TABLE `ccb_lists` (
`prop` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`data` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crews`
--

CREATE TABLE `crews` (
`id` int(11) NOT NULL,
`data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `geocodes`
--

CREATE TABLE `geocodes` (
`address` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`geocode` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `individuals`
--

CREATE TABLE `individuals` (
`id` int(11) NOT NULL,
`data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application_variables`
--
ALTER TABLE `application_variables`
ADD PRIMARY KEY (`prop`),
ADD UNIQUE KEY `prop` (`prop`);

--
-- Indexes for table `ccb_lists`
--
ALTER TABLE `ccb_lists`
ADD PRIMARY KEY (`prop`),
ADD UNIQUE KEY `prop` (`prop`);

--
-- Indexes for table `crews`
--
ALTER TABLE `crews`
ADD PRIMARY KEY (`id`),
ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `geocodes`
--
ALTER TABLE `geocodes`
ADD UNIQUE KEY `address` (`address`);

--
-- Indexes for table `individuals`
--
ALTER TABLE `individuals`
ADD PRIMARY KEY (`id`),
ADD UNIQUE KEY `id` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
```
