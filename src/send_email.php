<?php

    require_once 'config.php';
    /**
     * The CrewEmail class provides methods for sending confirmation or error emails
     * related to joining a crew.
     */
    class CrewEmail {
        /**
         * Sends a confirmation email or an error email if there was an error.
         *
         * @param string $emailBody The body of the email.
         * @param string $name The name of the recipient.
         * @param string $toEmail The email address of the recipient.
         * @param string $phone The phone number of the recipient.
         * @param string $birthdate The birthdate of the recipient.
         * @param string $gender The gender of the recipient.
         * @param int $crewId The ID of the crew.
         * @param string|null $error The error message if there was an error.
         * 
         * @return void
         */
        static public function sendEmail($emailBody, $name, $toEmail, $phone, $birthdate, $gender, $crewId, $error = null) {
            $emailSubject = "Thanks for joining a Crew!";
            $headers = [
                'MIME-Version: 1.0',
                'Content-type: text/html; charset=iso-8859-1',
                'From: ' . FROM_EMAIL_ADDRESS
            ];
    
            if ($error) {
                $emailSubject = "Oops your Crew submission failed.";
                $emailBody = "We want you in our Crew! " .
                             "Please visit " . INFORMATION_WEBSITE . " to sign up again.<br><br>" .
                             "If you are having difficulties, please email " . FROM_EMAIL_ADDRESS . ".<br><br>" .
                             "Crew Team<br><br>";
    
                $emailInformation = "<br><br>$name<br>$toEmail<br>$phone<br>$birthdate<br>$gender<br>" . SITE_ADDRESS . INSTALL_DIRECTORY . "?id=$crewId<br><br>";
    
                self::sendMail(FROM_EMAIL_ADDRESS, $emailSubject, $emailBody . $emailInformation . $error, $headers);
            }
    
            if ($toEmail) {
                self::sendMail($toEmail, $toEmailSubject, $emailBody, $headers);
            }
        }
    
        static private function sendMail($to, $subject, $body, $headers) {
            $headersString = implode("\r\n", $headers);
            if (!mail($to, $subject, $body, $headersString)) {
                throw new Exception("Failed to send email to $to");
            }
        }
    }
?>