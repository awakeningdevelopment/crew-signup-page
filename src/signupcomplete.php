<?php
    // Include necessary files
    require_once 'config.php';
    require_once 'send_email.php';

    // Initialize error variable
    $error = '';

    // Extract POST parameters
    $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
    $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
    $email_toaddress = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
    $birthdate = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_STRING);
    $gender = strtoupper(filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING)); // Response is expected in all caps
    $crewid = filter_input(INPUT_POST, 'crewid', FILTER_SANITIZE_STRING);

    // Format birthdate using DateTime
    $birthdateObj = DateTime::createFromFormat('Y-m-d', $birthdate);
    $birthDateString = $birthdateObj ? $birthdateObj->format('Y-m-d') : '';

    // Create PHP object for form submission
    $payload = [
        'profile' => [
            'fields' => [
                ['field_id' => CREWS_FORM_BIRTHDATE_FIELD_ID, 'answer' => $birthDateString],
                ['field_id' => CREWS_FORM_GENDER_FIELD_ID, 'answer' => $gender],
                ['field_id' => CREWS_FORM_FIRSTNAME_FIELD_ID, 'answer' => $firstname],
                ['field_id' => CREWS_FORM_LASTNAME_FIELD_ID, 'answer' => $lastname],
                ['field_id' => CREWS_FORM_EMAIL_FIELD_ID, 'answer' => $email_toaddress],
                ['field_id' => CREWS_FORM_PHONE_FIELD_ID, 'answer' => $phone]
            ]
        ],
        'answers' => [
            ['question_id' => CREWS_FORM_QUESTION_ID, 'answer' => $crewid]
        ],
        'original_respondent_id' => 0
    ];

    // Convert payload to JSON
    $json_payload = json_encode($payload);

    // Initialize cURL session
    $ch = curl_init();

    // Set cURL options
    curl_setopt($ch, CURLOPT_URL, 'https://' . CHURCH_DOMAIN . '.ccbchurch.com/api/forms/' . CREWS_FORM_ID . '/responses');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Execute cURL request
    $response = curl_exec($ch);

    // Check for errors
    if ($response === false) {
        $error = 'cURL error: ' . curl_error($ch);
    } else {
        // Decode JSON response
        $json_output = json_decode($response);

        if(!$json_output->confirmation_code) {
            $error = $response;
        } elseif($json_output->confirmation_text) {
            $email_body = $json_output->confirmation_text;
        }
    }

    // Close cURL session
    curl_close($ch);

    // Send email on error - since CCB sends confirmation emails itself
    if ($error) {
        CrewEmail::sendEmail($email_body, "$firstname $lastname", $email_toaddress, $phone, $birthDateString, $gender, $crewid, $error);
    }

    // Output API response
    echo $response;
?>
