<?php
    /**
     * Simple function to convert arguments passed as server cron arguments to GET and POST arguments
     */
    if (!isset($_SERVER["HTTP_HOST"])) {
        parse_str($argv[1], $_GET);
        parse_str($argv[1], $_POST);
    }
?>