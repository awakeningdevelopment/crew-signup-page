<?php

    require_once 'cron_arguments.php';
    require_once '../config.php';
    require_once '../ccb_api.php';
    require_once '../database.php';
    require_once '../send_email.php';

    if (!isset($_POST["key"]) || ($_POST["key"] !== API_KEY)) {
        die("Error - Missing or invalid API key");
    }

    $ccb = new CCB;
    $database = new Database;
    $app_vars = new AppVars($database->db);

    $noError = true;
    $form_responses_modified_since = $app_vars->getVar('form_responses_modified_since')->var;

    if (!validateDate($form_responses_modified_since)) {
        die("Error - form_responses_modified_since date invalid");
    }

    $form_detail_api_response = $ccb->ccbAPICall('form_detail', array('id' => CREWS_FORM_ID));
    $form = $form_detail_api_response->response->forms[0]->form;
    $confirmation_text = $form->confirmationText ?? '';

    $api_response = $ccb->ccbAPICall('form_responses', array('form_id' => CREWS_FORM_ID, 'modified_since' => $form_responses_modified_since));
    $form_responses = $api_response->response->form_responses->form_response;

    foreach ($form_responses as $form_response) {
        $individual_id = (string) $form_response->individual->attributes()->id;

        if (!in_array($individual_id, BLOCKLIST)) {
            $individual_name = (string) $form_response->individual;
            $crew_id = (string) $form_response->answers->answer_value;

            $profile_info_list = $form_response->profile_fields->profile_info;

            foreach ($profile_info_list as $profile_info) {
                switch ((string) $profile_info["name"]) {
                    case 'birthday':
                        $profile_age = (string) $profile_info[0];
                        break;
                    case 'email_primary':
                        $profile_email = (string) $profile_info[0];
                        break;
                    case 'phone_mobile':
                        $profile_phone = (string) $profile_info[0];
                        break;
                }
            }

            $r = $ccb->ccbAPICall('add_individual_to_group', array('id' => $individual_id, 'group_id' => $crew_id, 'status' => 'add')); 
            $error = (string) $r->response->errors->error[0];

            CrewEmail::sendEmail('<html><body>'.$confirmation_text.'</body></html>', $individual_name, $profile_email, $profile_phone, $profile_age, $crew_id, $error);

            if ($error) {
                $noError = false;
                error_log("Error - $error");
            }

            if (strcmp($form_response->modified, $form_responses_modified_since) > 0) {
                $form_responses_modified_since = (string) $form_response->modified;
            }
        }
    }

    $app_vars->addVar('form_responses_modified_since', $form_responses_modified_since);

    function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
?>