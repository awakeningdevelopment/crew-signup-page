<?php
    /*
    * Dependencies
    */
    require_once 'cron_arguments.php';
    require_once '../config.php';
    require_once '../ccb_api.php';
    require_once '../database.php';
    require_once '../models/crew.php';
    require_once '../models/individual.php';
    require_once '../models/ccb_list.php';

    if (!isset($_POST["key"]) || ($_POST["key"] !== API_KEY)) {
        die("Error - Missing or invalid API key");
    }

    $ccb = new CCB;
    $database = new Database;
    $appVars = new AppVars($database->db);
    $crewsTable = new Crew($database->db);
    $individualTable = new Individual($database->db);
    $ccbListTable = new CCBList($database->db);

    // Retrieve the last successful run date
    $crewsModifiedSince = $appVars->getVar('crews_modified_since')->var;

    // Validate the date
    if (!validateDate($crewsModifiedSince)) {
        throw new Exception("Error - crews_modified_since date is invalid");
    }

    // Retrieve crews modified since the last successful run date
    $crews = $ccb->groupsInDepartment(CREWS_DEPARTMENT_NAME, $crewsModifiedSince);

    // Process each crew
    foreach ($crews as $crew) {
        $crewId = $crew->attributes()->id;
        $inactive = $crew->inactive === "true";
        $isOpen = $crew->membership_type === "Open to All";

        // If the group is inactive or not open to all, delete it from the cache
        if ($inactive || !$isOpen) {
            $crewsTable->deleteCrew($crewId);
            continue;
        }

        // Cache the updated crew profile
        $crewFullProfile = $ccb->groupForID($crewId);
        $crewsTable->addCrew($crewId, $crewFullProfile->asXML());

        // Cache the main leader's profile and image
        $crewMainLeader = $crewFullProfile->main_leader;
        $crewMainLeaderId = $crewMainLeader->attributes()->id;
        $individualProfile = $ccb->individualForID($crewMainLeaderId);
        $individualTable->addIndividual($crewMainLeaderId, $individualProfile->asXML());
        Individual::cacheIndividualImage($individualProfile);

        // Cache profiles and images of assistant leaders, if any
        $crewOtherLeaders = $crewFullProfile->leaders->leader;
        if ($crewOtherLeaders) {
            foreach ($crewOtherLeaders as $crewLeader) {
                $crewLeaderId = $crewLeader->attributes()->id;
                $individualProfile = $ccb->individualForID($crewLeaderId);
                if ($individualProfile) {
                    $individualTable->addIndividual($crewLeaderId, $individualProfile->asXML());
                    Individual::cacheIndividualImage($individualProfile);
                }
            }
        }

        // Update the most recent modified crew date
        $crewModified = (string) $crew->modified;
        if (strcmp($crewModified, $crewsModifiedSince) > 0) {
            $crewsModifiedSince = $crewModified;
        }
    }

    // Store the most recent modified crew date
    $appVars->addVar('crews_modified_since', $crewsModifiedSince);

    // Define an array of API calls and their corresponding list names
    $apiCalls = [
        'campus_list',
        'udf_grp_pulldown_3_list',
        'udf_grp_pulldown_1_list',
        'group_type_list'
    ];

    // Process each API call
    foreach ($apiCalls as $listName) {
        // Make the API call
        $response = $ccb->ccbAPICall($listName);

        // Check if the response is valid
        if ($response !== false && isset($response->response->items)) {
            // Extract the data and store it in XML format
            if ($listName === 'campus_list' && isset($response->response->campuses)) {
                $listData = $response->response->campuses->asXML();
            } else {
                $listData = $response->response->items->asXML();
            }

            // Add the list data to the database table
            $ccbListTable->addCCBList($listName, $listData);
        }
    }

    function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
?>