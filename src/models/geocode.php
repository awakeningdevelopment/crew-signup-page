<?php
//require_once '../config.php';

class GeoCode
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    /**
     * Get all GeoCodes from database
     */
    private function getGeoCodes()
    {
        $sql = "SELECT address, geocode FROM geocodes";
        $query = $this->db->prepare($sql);
        $query->execute();
        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php!
        return $query->fetchAll();
    }
    /**
     * Add a geocodes to database
     * @param int $address address of GeoCode
     * @param string $geocode geocode
     */
    private function addGeoCode($address, $geocode)
    {
        $sql = "INSERT INTO geocodes (address, geocode) VALUES (:address, :geocode) ON DUPLICATE KEY UPDATE geocode=:geocode;";
        $query = $this->db->prepare($sql);
        $parameters = array(':address' => $address, ':geocode' => $geocode);
        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        $query->execute($parameters);
    }
    /**
     * Delete a geocodes in the database
     * @param int $address address of GeoCode
     */
    private function deleteGeoCode($address)
    {
        $sql = "DELETE FROM geocodes WHERE address = :address";
        $query = $this->db->prepare($sql);
        $parameters = array(':address' => $address);

        $query->execute($parameters);
    }
    /**
     * Get a geocodes from database
     * @param int $address address of GeoCode
     */
    private function getGeoCode($address)
    {
        $sql = "SELECT address, geocode FROM geocodes WHERE address = :address LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':address' => $address);

        $query->execute($parameters);
        // fetch() is the PDO method that get exactly one result
        return $query->fetch();
    }
    /**
     * Update a geocodes in database
     * @param int $address address of GeoCode
     * @param string $geocode geocode
     */
    private function updateGeoCode($address, $geocode)
    {
        $sql = "UPDATE geocodes SET geocode = :geocode WHERE address = :address";
        $query = $this->db->prepare($sql);
        $parameters = array(':geocode' => $geocode, ':address' => $address);

        $query->execute($parameters);
    }
    /**
     * Get simple "stats".
     */
    private function getAmountOfGeoCodes()
    {
        $sql = "SELECT COUNT(address) AS amount_of_GeoCodes FROM geocodes";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->amount_of_GeoCodes;
    }
    /**
     * Get the GeoCode JSON response from the Google API
     */
    private function fetchGeoCodeJSONFromGoogle($address)
    {
        $url = sprintf("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s", urlencode($address), GEOCODE_API_KEY);
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Disabling SSL verification for simplicity. Make sure to handle this properly in production.
        
        $response = curl_exec($curl);
        curl_close($curl);
        
        return $response;
    }

    /**
     * Public function to get a geocode from database or fetch it from Google Maps API & cache if not already cached
     * @param int $address address of GeoCode
     */
    public function getGeoCodeForAddress($address)
    {
        $geocode_entry = self::getGeoCode($address);
        if (!$geocode_entry) {
            $geocode_json = self::fetchGeoCodeJSONFromGoogle($address);
            $geocode = json_decode($geocode_json);
            if (!is_null($geocode) && $geocode->status === "OK") {
                self::addGeoCode($address, $geocode_json);
            } else {
                //TODO: add retry cycle
                return;
            }
        } else {
            $geocode_json = $geocode_entry->geocode;
        }
        return $geocode_json;
    }



}