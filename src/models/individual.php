<?php
class Individual
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    /**
     * Get all individuals from database
     */
    public function getAllIndividuals()
    {
        $sql = "SELECT id, data FROM individuals";
        $query = $this->db->prepare($sql);
        $query->execute();
        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php!
        return $query->fetchAll();
    }
    /**
     * Add a individual to database
     * @param string $individual_id Id
     * @param string $data Data
     */
    public function addIndividual($individual_id, $data)
    {
        $sql = "INSERT INTO individuals (id, data) VALUES (:individual_id, :data) ON DUPLICATE KEY UPDATE data=:data;";
        $query = $this->db->prepare($sql);
        $parameters = array(':individual_id' => $individual_id, ':data' => $data);
        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        $query->execute($parameters);
    }
    /**
     * Delete a individual in the database
     * @param int $individual_id Id of individual
     */
    public function deleteIndividual($individual_id)
    {
        $sql = "DELETE FROM individuals WHERE id = :individual_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':individual_id' => $individual_id);

        $query->execute($parameters);
    }
    /**
     * Get a individual from database
     */
    public function getIndividual($individual_id)
    {
        $sql = "SELECT id, data FROM individuals WHERE id = :individual_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':individual_id' => $individual_id);

        $query->execute($parameters);
        // fetch() is the PDO method that get exactly one result
        return $query->fetch();
    }
    private function getImageSrcForIndividual($individual_id)
    {
        $individual = $this->getIndividual($individual_id);
        $individual = new SimpleXMLElement($individual->data);

        return (string) $individual->image;
    }
    public function getCachedImageSrcForIndividual($individual_id)
    {
        try {
            $image_src_url = $this->getImageSrcForIndividual($individual_id);
            $cache_image_url = SITE_ADDRESS.INSTALL_DIRECTORY.'image_cache/'.hash('md5', explode('?', $image_src_url)[0]);
        } catch (Exception $e) {
            $cache_image_url = "";
        }

        return $cache_image_url;
    }
    /**
     * Update a individual in database
     * @param string $individual_id Id
     * @param string $data Data
     * @param string $link Link
     * @param int $individual_id Id
     */
    public function updateIndividual($individual_id, $data)
    {
        $sql = "UPDATE individuals SET data = :data WHERE id = :individual_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':data' => $data, ':individual_id' => $individual_id);

        $query->execute($parameters);
    }
    /**
     * Get simple "stats".
     */
    public function getAmountOfIndividuals()
    {
        $sql = "SELECT COUNT(id) AS amount_of_individuals FROM individuals";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->amount_of_individuals;
    }

    public static function cacheIndividualImage($individual_profile) {

        $input = (string) $individual_profile->image;
        $output = DOCUMENT_ROOT.'/'.INSTALL_DIRECTORY.'image_cache/'.hash('md5', explode('?', $input)[0]);//remove url parameters and hash as filename
        $image_data = self::get_data($input);
        if($image_data !== false AND !empty($image_data)) {
            file_put_contents($output, $image_data);
        }
    }

    private static function get_data($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}