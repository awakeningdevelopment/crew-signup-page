<?php
class Crew
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    /**
     * Get all crews from database
     */
    public function getAllCrews()
    {
        $sql = "SELECT id, data FROM crews";
        $query = $this->db->prepare($sql);
        $query->execute();
        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php!
        return $query->fetchAll();
    }
    public function getAllCrewsAsSimpleXML()
    {
        $crews = $this->getAllCrews();
        foreach($crews as $key => $entry) {
            $crews[$key] = new SimpleXMLElement($entry->data);
        }
        return $crews;
    }
    /**
     * Add a crew to database
     * @param string $crew_id Id
     * @param string $data Data
     */
    public function addCrew($crew_id, $data)
    {
        $sql = "INSERT INTO crews (id, data) VALUES (:crew_id, :data) ON DUPLICATE KEY UPDATE data=:data;";
        $query = $this->db->prepare($sql);
        $parameters = array(':crew_id' => $crew_id, ':data' => $data);
        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        $query->execute($parameters);
    }
    /**
     * Delete a crew in the database
     * @param int $crew_id Id of crew
     */
    public function deleteCrew($crew_id)
    {
        $sql = "DELETE FROM crews WHERE id = :crew_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':crew_id' => $crew_id);

        $query->execute($parameters);
    }
    /**
     * Get a crew from database
     */
    public function getCrew($crew_id)
    {
        $sql = "SELECT crew_id, data FROM crews WHERE id = :crew_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':crew_id' => $crew_id);

        $query->execute($parameters);
        // fetch() is the PDO method that get exactly one result
        return $query->fetch();
    }
    /**
     * Update a crew in database
     * @param string $crew_id Id
     * @param string $data Data
     * @param string $link Link
     * @param int $crew_id Id
     */
    public function updateCrew($crew_id, $data)
    {
        $sql = "UPDATE crews SET data = :data WHERE id = :crew_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':data' => $data, ':crew_id' => $crew_id);

        $query->execute($parameters);
    }
    /**
     * Get simple "stats".
     */
    public function getAmountOfCrews()
    {
        $sql = "SELECT COUNT(id) AS amount_of_crews FROM crews";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->amount_of_crews;
    }

    /**
     * Alphabetical sorting comparison function for crews
     */

    static function crewAlphabeticalSort($a, $b){
        return strcmp($a->name, $b->name);
    }
    
    
    /*******************************************************
     *
     * Returns whether a crew is open and not full
     *
     *******************************************************/
    
    static function isCrewOpenAndNotFull($crew){
        $isOpen = self::is_true(strcmp($crew->membership_type,"Open to All") == 0);
        
        $notFull = ((int)$crew->group_capacity > (int)$crew->current_members) || self::is_true(strcmp($crew->group_capacity,"Unlimited") == 0);
        
        return $isOpen && $notFull;
    }

    private static function is_true($string) {
        return filter_var($string, FILTER_VALIDATE_BOOLEAN);
    }

    /*******************************************************
     *
     * Returns the specified custom defined field
     *
     *******************************************************/
    
    static function customFieldWithLabel($crew, $testLabel){
        $userDefinedField = $crew->user_defined_fields->user_defined_field;
        for ($i = 0; $i < count($userDefinedField); $i++) {
            $label = $userDefinedField[$i]->label;
            
            if (strcmp($label,$testLabel) == 0)
                return $userDefinedField[$i]->selection;
        }
        return null;
    }

    static function reformatLocationAddress($address){
        $returnString = '';
        if (!empty($address->street_address)) { $returnString = $returnString.$address->street_address.'<br>';}
        if (!empty($address->city)) { $returnString = $returnString.$address->city.', ';}
        if (!empty($address->state)) { $returnString = $returnString.$address->state.' ';}
        if (!empty($address->zip)) { $returnString = $returnString.$address->zip;}
        return $returnString;
    }

}