<?php
class AppVars
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    /**
     * Get all Vars from database
     */
    public function getVars()
    {
        $sql = "SELECT prop, var FROM application_variables";
        $query = $this->db->prepare($sql);
        $query->execute();
        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php!
        return $query->fetchAll();
    }
    /**
     * Add a application_variables to database
     * @param int $prop prop of Var
     * @param string $var var
     */
    public function addVar($prop, $var)
    {
        $sql = "INSERT INTO application_variables (prop, var) VALUES (:prop, :var) ON DUPLICATE KEY UPDATE var=:var;";
        $query = $this->db->prepare($sql);
        $parameters = array(':prop' => $prop, ':var' => $var);
        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        $query->execute($parameters);
    }
    /**
     * Delete a application_variables in the database
     * @param int $prop prop of Var
     */
    public function deleteVar($prop)
    {
        $sql = "DELETE FROM application_variables WHERE prop = :prop";
        $query = $this->db->prepare($sql);
        $parameters = array(':prop' => $prop);

        $query->execute($parameters);
    }
    /**
     * Get a application_variables from database
     * @param int $prop prop of Var
     */
    public function getVar($prop)
    {
        $sql = "SELECT prop, var FROM application_variables WHERE prop = :prop LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':prop' => $prop);

        $query->execute($parameters);
        // fetch() is the PDO method that get exactly one result
        return $query->fetch();
    }
    /**
     * Update a application_variables in database
     * @param int $prop prop of Var
     * @param string $var var
     */
    public function updateVar($prop, $var)
    {
        $sql = "UPDATE application_variables SET var = :var WHERE prop = :prop";
        $query = $this->db->prepare($sql);
        $parameters = array(':var' => $var, ':prop' => $prop);

        $query->execute($parameters);
    }
    /**
     * Get simple "stats".
     */
    public function getAmountOfVars()
    {
        $sql = "SELECT COUNT(prop) AS amount_of_Vars FROM application_variables";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->amount_of_Vars;
    }
}