<?php
class CCBList
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    /**
     * Get all ccb_lists from database
     */
    public function getAllCCBLists()
    {
        $sql = "SELECT prop, data FROM ccb_lists";
        $query = $this->db->prepare($sql);
        $query->execute();
        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php!
        return $query->fetchAll();
    }
    /**
     * Add a ccb_list to database
     * @param string $ccb_list_prop Key
     * @param string $data Data
     */
    public function addCCBList($ccb_list_prop, $data)
    {
        $sql = "INSERT INTO ccb_lists (prop, data) VALUES (:ccb_list_prop, :data) ON DUPLICATE KEY UPDATE data=:data;";
        $query = $this->db->prepare($sql);
        $parameters = array(':ccb_list_prop' => $ccb_list_prop, ':data' => $data);
        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        $query->execute($parameters);
    }
    /**
     * Delete a ccb_list in the database
     * @param int $ccb_list_prop Key of ccb_list
     */
    public function deleteCCBList($ccb_list_prop)
    {
        $sql = "DELETE FROM ccb_lists WHERE prop = :ccb_list_prop";
        $query = $this->db->prepare($sql);
        $parameters = array(':ccb_list_prop' => $ccb_list_prop);

        $query->execute($parameters);
    }
    /**
     * Get a ccb_list from database
     */
    public function getCCBList($ccb_list_prop)
    {
        $sql = "SELECT prop, data FROM ccb_lists WHERE prop = :ccb_list_prop LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':ccb_list_prop' => $ccb_list_prop);

        $query->execute($parameters);
        // fetch() is the PDO method that get exactly one result
        return $query->fetch();
    }
    public function getCCBListAsSimpleXML($ccb_list_prop)
    {
        $ccb_list = $this->getCCBList($ccb_list_prop);
        $ccb_list = new SimpleXMLElement($ccb_list->data);

        return $ccb_list;
    }
    /**
     * Update a ccb_list in database
     * @param string $ccb_list_prop Key
     * @param string $data Data
     * @param string $link Link
     * @param int $ccb_list_prop Key
     */
    public function updateCCBList($ccb_list_prop, $data)
    {
        $sql = "UPDATE ccb_lists SET data = :data WHERE prop = :ccb_list_prop";
        $query = $this->db->prepare($sql);
        $parameters = array(':data' => $data, ':ccb_list_prop' => $ccb_list_prop);

        $query->execute($parameters);
    }
    /**
     * Get simple "stats".
     */
    public function getAmountOfCCBLists()
    {
        $sql = "SELECT COUNT(prop) AS amount_of_ccb_lists FROM ccb_lists";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->amount_of_ccb_lists;
    }
}