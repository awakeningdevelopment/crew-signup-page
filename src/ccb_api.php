<?php
    /*
    * Dependencies
    */
    require_once 'config.php';
    require_once 'database.php';
    require_once 'models/application_variables.php';

    class CCB {
        protected $baseAPIUrl;
        protected $database;
        protected $appVars;

        function __construct()
        {
            try {
                $this->database = new Database;
            } catch (PDOException $e) {
                exit('Error: Database connection could not be established.');
            }

            $this->appVars = new AppVars($this->database->db);
            $this->baseAPIUrl = 'https://' . CHURCH_DOMAIN . '.ccbchurch.com/api.php';
        }

        public function ccbAPICall($serviceName, $getParams = [], $postParams = []) {
            $this->checkConfiguration();
            $this->checkRateLimit();
    
            $url = $this->constructUrl($serviceName, $getParams);
            $output = $this->executeCurl($url, $postParams);
    
            return $this->parseResponse($output);
        }
    
        private function checkConfiguration() {
            if (empty(CCB_API_USER) || empty(CCB_API_PASSWORD) || empty($this->baseAPIUrl)) {
                throw new Exception('Error: CCB API details missing.');
            }
        }
    
        private function checkRateLimit() {
            try {
                $rateLimitReset = $this->appVars->getVar('ratelimit_reset')->var;
                if (!is_numeric($rateLimitReset)) {
                    throw new Exception('Error: ratelimit_reset is not a valid numeric value.');
                }
        
                $rateLimitReset = floatval($rateLimitReset); // Convert to float if necessary
        
                if ($rateLimitReset > time()) {
                    $success = time_sleep_until($rateLimitReset);
                    if ($success !== true || $rateLimitReset > time()) {
                        throw new Exception('Error: Unable to sleep until rate limit reset.');
                    }
                }
            } catch (Exception $e) {
                //bad time data in database - repair with + 5 current seconds
                $this->appVars->addVar('ratelimit_reset', time()+5);
            }
        }
    
        private function constructUrl($serviceName, $getParams) {
            $queryString = http_build_query($getParams);
            return $this->baseAPIUrl . '?srv=' . $serviceName . '&' . $queryString;
        }
    
        private function executeCurl($url, $postParams) {
            $curl = curl_init();
            $headers = [];
            curl_setopt_array($curl, [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => !empty($postParams),
                CURLOPT_POSTFIELDS => http_build_query($postParams),
                CURLOPT_USERPWD => CCB_API_USER . ':' . CCB_API_PASSWORD,
                CURLOPT_SSL_VERIFYPEER => false, // Ignore SSL verification for simplicity
            ]);
            // this function is called by curl for each header received
            curl_setopt($curl, CURLOPT_HEADERFUNCTION,
                function($curl, $header) use (&$headers)
                {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $headers[strtolower(trim($header[0]))] = trim($header[1]);
                
                return $len;
                }
            );
    
            $output = curl_exec($curl);
    
            if ($output === false) {
                throw new Exception('Error: cURL request failed: ' . curl_error($curl));
            }
    
            $rateLimitReset = $headers["x-ratelimit-reset"];
            $this->appVars->addVar('ratelimit_reset', $rateLimitReset);
    
            curl_close($curl);
            return $output;
        }
    
        private function parseResponse($output) {
            $xmlStartPos = stripos($output, "<?xml");
            if ($xmlStartPos === false) {
                throw new Exception('Error: Invalid response format.');
            }
    
            $xml = substr($output, $xmlStartPos);
            $simpleXml = new SimpleXMLElement($xml);

            // Check for errors in the response
            if (isset($simpleXml->response->errors->error)) {
                $errors = [];
                foreach ($simpleXml->response->errors->error as $errorElement) {
                    $error = (string)$errorElement->error;
                    $errors[] = $error;
                }
                $error_message = implode("; ", $errors);
                throw new Exception('API Error: ' . $error_message);
            }

            return $simpleXml;
        }


        /*******************************************************
         *
         * Returns groups filtered to the Crews department
         *
         *******************************************************/
        
        function groupsInDepartment($department_filter, $modified_since = null, $include_image_link = false){
            $groupprofilesr = $this->ccbAPICall('group_profiles',array('include_participants' => "false", 'modified_since' => $modified_since,'include_image_link' => $include_image_link));

            $groups = $groupprofilesr->response->groups;

            // Debugging: output groups
            if ($groups === null) {
                die('Error: No groups found.');
            } elseif (!isset($groups->group)) {
                die('Error: No groups element found.');
            }
            
            $filteredgroups = array();
            for ($i = 0; $i < count($groups->group); $i++) {
                $group = $groups->group[$i];
                $department = $group->department;
                //$inactive = $group->inactive;
                if ((strcmp($department, (string) $department_filter) == 0))// && (strcmp($inactive,"false") == 0))
                    $filteredgroups[] = $group;
            }
            return $filteredgroups;
        }

        /*******************************************************
         *
         * Returns group for ID
         *
         *******************************************************/
        
        function groupForID($id, $include_image_link = true){
            $groupprofiler = $this->ccbAPICall('group_profile_from_id',array('id' => (string) $id, 'include_image_link' => $include_image_link));
            
            $newgroup = $groupprofiler->response->groups->group[0];
            
            return $newgroup;
        }

        /*******************************************************
         *
         * Returns individual for ID
         *
         *******************************************************/
        
        function individualForID($id){
            $individualprofiler = $this->ccbAPICall('individual_profile_from_id',array('individual_id' => (string) $id));

            $newindividual = $individualprofiler->response->individuals->individual[0];
            
            return $newindividual;
        }

    }
?>