function checkRequiredFields() {
    let missingField = false;
    const inputs = Array.from(document.querySelectorAll("input, textarea"));

    inputs.forEach(input => {
        const isRequired = input.hasAttribute("required");

        if (isRequired && !input.value.trim()) {
            input.parentElement.classList.add("has-error");
            missingField = true;
        } else {
            input.parentElement.classList.remove("has-error");
        }
    });

    const formAlert = document.getElementById("formalert");
    formAlert.textContent = missingField ? "You are missing required fields!" : "";

    return missingField;
}

function SubmitFormAsync() {
    //Serialize the data
    var missingField = checkRequiredFields();
    if (!missingField) {
        var data = $('#emailform').serialize();
        
        $.post('src/signupcomplete.php', data, function(response){
               // do something here on success
               
               });
        
        $('#emailModal').modal('hide');
        $('#emailModal').find('form')[0].reset();
        
        $('#successModal').modal('show');
    }
}

$('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
    });

$('.updateModelInfo').on('show.bs.modal', function (event) {
         var button = $(event.relatedTarget) // Button that triggered the modal
         var crewid = button.data('crewid') // Extract info from data-* attributes
         var crewname = button.data('crewname') // Extract info from data-* attributes
         var crewdescription = button.data('crewdescription') // Extract info from data-* attributes

         var modal = $(this)
         modal.find('.modal-title').text('Join ' + crewname)
         modal.find('.modal-description').text(crewdescription)
         modal.find('#crewid').val(crewid)
         })


function escapeString(inputstring) {
    return inputstring.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

const getUrlParameter = (sParam) => {
    const searchParams = new URLSearchParams(window.location.search);
    return searchParams.has(sParam) ? searchParams.get(sParam) : null;
};

var allMarkers = [];

function applyFilter() {
    const rex_id = new RegExp("\\b" + escapeString($('#filterid').val()), 'i');
    const rex_name = new RegExp("\\b" + escapeString($('#filtername').val()), 'i');
    const rex_location = new RegExp(escapeString($('#filterlocation').val()), 'i');
    //const rex_campus = new RegExp("\\b" + escapeString($('#filtercampus option:selected').text()), 'i');
    //const rex_grouptype = new RegExp("\\b" + escapeString($('#filtergrouptype option:selected').text()), 'i');
    const rex_crewtype = new RegExp("\\b" + escapeString($('#filtercrewtype option:selected').text()), 'i');
    const rex_gender = new RegExp("\\b" + escapeString($('#filtergender option:selected').text()), 'i');
    const rex_date = new RegExp("\\b" + escapeString($('#filterday option:selected').text()), 'i');

    $('.searchable .filter').hide().removeClass('alt');

    let alt = 0;

    $('.searchable .filter').each(function(index) {
        const row = $(this);
        const marker = allMarkers[index];
        const output = rex_id.test(row.find('.id').text()) &&
                       rex_name.test(row.find('.name').text()) &&
                       rex_location.test(row.find('.address').text()) &&
                       //rex_campus.test(row.find('.campus').text()) &&
                       //rex_grouptype.test(row.find('.grouptype').text()) &&
                       rex_crewtype.test(row.find('.crewtype').text()) &&
                       rex_gender.test(row.find('.gender').text()) &&
                       rex_date.test(row.find('.day').text());

        if (output) {
            if (alt++ % 2 === 1) {
                row.addClass('alt');
            }
            if (marker) {
                marker.setVisible(true);
            }
            row.show();
        } else {
            if (marker) {
                marker.setVisible(false);
            }
        }
    });
}

$(document).ready(() => {
    // Hide panel-collapse elements if window width is less than or equal to 768
    if ($(window).width() <= 768) {  
        $('.panel-collapse').removeClass('in');
    }

    // Function to set filters based on GET parameters and apply them
    const setFilterAndApply = (filterId, parameterName) => {
        const parameterValue = getUrlParameter(parameterName);
        if (parameterValue) {
            $(filterId).val(unescape(parameterValue)).change();
            applyFilter();
        }
    };

    // Preset filters from GET parameters
    ['id', 'name', 'location', 'campus', 'crewtype', 'grouptype', 'gender', 'day']
        .forEach(parameter => setFilterAndApply(`#filter${parameter}`, parameter));

    // Event handlers to apply filter on filter changes
    $('#filterid, #filtername, #filterlocation, #filtercampus, #filtercrewtype, #filtergrouptype, #filtergender, #filterday')
        .on('keyup change', () => applyFilter());
});


const openShare = async (pageID, day, time, location) => {
    if (navigator.share) {
        await shareNative(pageID, day, time, location);
    } else {
        shareFallback(pageID);
    }
};

const shareNative = async (pageID, day, time, location) => {
    const shareUrl = `${window.location.protocol}//${window.location.host}/crews-signup?id=${pageID}`;

    try {
        await navigator.share({
            text: `Join my Crew! We meet ${day} ${time} @ ${location}.`,
            url: shareUrl,
        });
    } catch (error) {
        console.error('Error sharing via native share:', error);
    }
};

const shareFallback = (pageID) => {
    const shareUrl = `${window.location.protocol}//${window.location.host}/crews-signup?id=${pageID}`;

    try {
        window.open(shareUrl, '_self');
    } catch (error) {
        console.error('Error sharing via fallback:', error);
    }
};