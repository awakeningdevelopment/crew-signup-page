<?php
    /**
     * GENERAL CONSTANTS
     */

    define('API_KEY', '---------------------------------'); //must be passed as a post argument to services, should be unique to each install
    define('FROM_EMAIL_ADDRESS', 'reply@example.com');
    define('INFORMATION_WEBSITE', 'example.com/info');
    define('SITE_ADDRESS', 'https://example.com/');
    define('INSTALL_DIRECTORY','ccb/');
    define('DOCUMENT_ROOT','/home/your_username/public_html');

    /**
     * CCB CONSTANTS
     */

    define('CHURCH_DOMAIN', 'yourchurch'); // your subdomain for ccbchurch
    define('CCB_API_USER', 'ccb_user'); // your CCB api username
    define('CCB_API_PASSWORD', 'ccb_password'); // your CCB api password

    define('CREWS_DEPARTMENT_NAME', 'Crews');

    define('CREWS_FORM_ID', '51');
    define('CREWS_FORM_BIRTHDATE_FIELD_ID', '51');
    define('CREWS_FORM_GENDER_FIELD_ID', '51');
    define('CREWS_FORM_FIRSTNAME_FIELD_ID', '51');
    define('CREWS_FORM_LASTNAME_FIELD_ID', '51');
    define('CREWS_FORM_EMAIL_FIELD_ID', '51');
    define('CREWS_FORM_PHONE_FIELD_ID', '51');
    define('CREWS_FORM_QUESTION_ID', '51');

    /**
     * BLOCKLIST FOR INDIVIDUAL IDS
     */

     define('BLOCKLIST', array(
        '0'
    ));

    /**
     * DATABASE CONSTANTS
     */

    define('DB_TYPE', 'mysql');
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'db_name');
    define('DB_USER', 'db_user');
    define('DB_PASS', 'db_password');
    define('DB_CHARSET', 'utf8');

    /**
     * GOOGLE MAPS CONSTANTS
     */

    define('GEOCODE_API_KEY', '---------------------------------'); //this should be separate from your javascript API key since given access to paid resources
    define('MAPS_API_KEY', '---------------------------------');

?>