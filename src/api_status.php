<?php
require_once 'config.php';
require_once 'database.php';
require_once 'ccb_api.php';

try {
    $ccb = new CCB;
    $statusResponse = $ccb->ccbAPICall('api_status');

    $dailyLimit = (string)$statusResponse->response->daily_limit;
    $lastRunDate = (string)$statusResponse->response->last_run_date;
    $counter = (string)$statusResponse->response->counter;

    echo json_encode([
        'daily_limit' => $dailyLimit,
        'last_run_date' => $lastRunDate,
        'counter' => $counter
    ]);
} catch (Exception $e) {
    echo json_encode(['error' => $e->getMessage()]);
}
?>